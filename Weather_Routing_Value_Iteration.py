"""
This file may not be shared/redistributed freely. Please read copyright notice in the git repo.
"""
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from gym.envs.toy_text import discrete

# action space available to the agent
N = 0
NE = 1
E = 2
SE = 3
S = 4
SW = 5
W = 6
NW = 7
STOP = 8


class GridworldEnv(discrete.DiscreteEnv):

    metadata = {'render.modes': ['human', 'ansi']}

    def __init__(self, shape=(15, 15)):
        if not isinstance(shape, (list, tuple)) or not len(shape) == 2:
            raise ValueError('shape argument must be a list/tuple of length 2')

        self.shape = shape

        # define size of state and action space
        nS = np.prod(shape)
        nA = 9
        self.ncol = shape[0]
        self.nrow = shape[1]
        isd = np.random.uniform(1, 1.5, nS)
        #print(isd)

        MAX_Y = shape[0]
        MAX_X = shape[1]

        P = {}
        grid = np.arange(nS).reshape(shape)
        it = np.nditer(grid, flags=['multi_index'])

        while not it.finished:
            s = it.iterindex
            y, x = it.multi_index

            # P[s][a] = (prob, next_state, reward, is_done)
            P[s] = {a: [] for a in range(nA)}

            is_done = lambda s: s == 0 or s == (nS - 1) or s == MAX_X - 1 or s == (MAX_Y - 1) * MAX_X
            reward = 0.0 if is_done(s) else -1.0

            # We're stuck in a terminal state
            if is_done(s):
                P[s][N] = [(isd[s], s, reward, True)]
                P[s][NE] = [(isd[s], s, reward, True)]
                P[s][E] = [(isd[s], s, reward, True)]
                P[s][SE] = [(isd[s], s, reward, True)]
                P[s][S] = [(isd[s], s, reward, True)]
                P[s][SW] = [(isd[s], s, reward, True)]
                P[s][W] = [(isd[s], s, reward, True)]
                P[s][NW] = [(isd[s], s, reward, True)]
                P[s][STOP] = [(isd[s], s, reward, True)]
            # Not a terminal state
            else:
                ns_n = s if y == 0 else s - MAX_X
                ns_ne = s if (y == 0 or x == (MAX_X - 1)) else s - (MAX_X - 1)
                ns_e = s if x == (MAX_X - 1) else s + 1
                ns_se = s if (y == (MAX_Y -1) or x == (MAX_X - 1)) else s + (MAX_X + 1)
                ns_s = s if y == (MAX_Y - 1) else s + MAX_X
                ns_sw = s if (x == 0 or y == (MAX_Y - 1)) else s + (MAX_X - 1)
                ns_w = s if x == 0 else s - 1
                ns_nw = s if (y == 0 or x == 0) else s - (MAX_X + 1)
                P[s][N] = [(isd[s], ns_n, reward, is_done(ns_n))]
                P[s][NE] = [(isd[s], ns_ne, reward, is_done(ns_ne))]
                P[s][E] = [(isd[s], ns_e, reward, is_done(ns_e))]
                P[s][SE] = [(isd[s], ns_se, reward, is_done(ns_se))]
                P[s][S] = [(isd[s], ns_s, reward, is_done(ns_s))]
                P[s][SW] = [(isd[s], ns_sw, reward, is_done(ns_sw))]
                P[s][W] = [(isd[s], ns_w, reward, is_done(ns_w))]
                P[s][NW] = [(isd[s], ns_nw, reward, is_done(ns_nw))]
                P[s][STOP] = [(1, s, reward, is_done(s))]

            it.iternext()

        super(GridworldEnv, self).__init__(nS, nA, P, isd)

def to_rc(s, ncol):
    col = s%ncol
    row = int(np.floor(s/ncol))
    return row, col

def plot_value_function(env, J, figsize=8):
    ncol = env.ncol  # columns.
    S = env.observation_space.n  # number of states (squares)
    nrow = S // ncol  # rows

    A = np.zeros((nrow, ncol))  # plot the value function as a matrix
    for i in range(S):
        nr, nc = to_rc(i, ncol)
        A[nr,nc] = np.round(J[i],decimals=2)

    if figsize is not None:
        plt.figure(figsize=(figsize, figsize))
    sns.heatmap(A, cmap="YlGnBu", annot=True, cbar=False, square=True,fmt='g')

def qs_(env, s, gamma, v):
    return {a: sum([p*(r+ (gamma*v[sp] if not done else 0)) for p, sp, r, done in env.P[s][a]]) for a in env.P[s].keys()}

def value_iteration(env, gamma=.99, theta=0.0001):
    n = env.observation_space.n
    if "env" in env.__dict__:
        env.P = env.env.P
    V = np.zeros(n)  # value function
    while True:
        delta = 0
        #print(V)
        for s in env.P.keys():
            v, V[s] = V[s], max(qs_(env, s, gamma, V).values()) if len(env.P[s]) > 0 else 0
            delta = max(delta, np.abs(v - V[s]))
        if delta < theta:
            break
    pi = values2policy(env, V, gamma)
    return pi, V

def values2policy(env, V, gamma):
    pi = {}
    for s, Ps in env.P.items():
        Q = {a: v - 1e-8 * a for a, v in qs_(env, s, gamma, V).items()}
        pi[s] = max(Q, key=Q.get)
        return pi

if __name__ == "__main__":
    env = GridworldEnv()
    policy, v = value_iteration(env,gamma=0.89)
    plot_value_function(env, v)
    plt.title("Value function obtained using value iteration to find optimal policy")
    plt.show()
