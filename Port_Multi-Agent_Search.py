
from agent import Agent, train
from pacman.pacman_environment import GymPacmanEnvironment
import numpy as np
import time

class MultiSearchProblem:
    def utility(self, state):
        return 0 # The utility (reward) of the state

    def terminal_test(self, state):
        return False # True if the state is a terminal state

    def get_players(self, state):
        # Return the number of players in the game. Player 0 is always the Agent, and 1, 2, ... are the opponents
        return 2 # a two-player game like chess

    def actions(self, state, player):
        return {0: 0.8, 1: 0.2}

    def get_successor(self, state, action, agentIndex):
        return state

class PacmanMultisearchModel(MultiSearchProblem):
    def __init__(self):
        self.num_expanded = 0

    def utility(self, state):
        return state.getScore()

    def terminal_test(self, state):
        return state.isWin() or state.isLose()

    def get_players(self, state):
        return state.getNumAgents()

    def actions(self, state, player):
        actions = state.getLegalActions(player)
        return {a: 1/len(actions) for a in actions}

    def get_successor(self, state, action, agentIndex):
        self.num_expanded += 1
        return state.generateSuccessor(agentIndex, action)

class MultiAgentSearchAgent(Agent): 
    def __init__(self, env, depth=2):
        self.depth = depth
        self.minimax_scores = []
        self.model = PacmanMultisearchModel()
        self.num_expanded = []
        super().__init__(env)

    def multisearch_evaluate(self, state):
        # Implement this function. Compute best (score, action)
        best_score, best_action = None, None
        return best_score, best_action

    def pi(self, gameState):
        self.model.num_expanded = 0
        self_score, action = self.multisearch_evaluate(gameState)
        self.minimax_scores.append(self_score)
        self.num_expanded.append(self.model.num_expanded)
        return action 

class GymMinimaxAgent(MultiAgentSearchAgent):
    def __init__(self, env, depth=1):
        super().__init__(env, depth)

    def multisearch_evaluate(self, gameState):
        return self.minimax(gameState, 0, self.depth) # return (optimal score, optimal action)

    def minimax(self, x, q: int, d: int): # q is current player, d is depth (same as in code)
        if d == 0 or self.model.terminal_test(x):
            return self.model.utility(x), None

        q_ = (q + 1) % x.getNumAgents()             # q' = q_: Next agent index (q=0 is you, q >= 1 other agents)
        d_ = d-1 if q == x.getNumAgents()-1 else d  # d' = d_: Decrease depth by one if q==Q-1 (i.e. all ghosts have played a round)

        V = {a: self.minimax(self.model.get_successor(x, a, q), q_, d_)[0] for a in self.model.actions(x, q)}
        best_action = max(V, key=V.get) if q == 0 else min(V, key=V.get) # I use a dictionary V[a] = expected utility.
        return V[best_action], best_action


class GymExpectimaxAgent(MultiAgentSearchAgent):
    def __init__(self, env, depth=1):
        super().__init__(env, depth)

    def multisearch_evaluate(self, state):
        return self.expectimax(state, 0, self.depth)

    def expectimax(self, x, q: int, d: int):
        if d == 0 or self.model.terminal_test(x):
            return self.model.utility(x), None

        q_ = (q + 1) % x.getNumAgents()                 # q' = q_: Next agent index (q=0 is you, q >= 1 other agents)
        d_ = d - 1 if q == x.getNumAgents() - 1 else d  # d' = d_: Decrease depth by one if q==Q-1 (i.e. all ghosts have played a round)

        V = {a: (self.expectimax(self.model.get_successor(x, a, q), q_, d_)[0], pW) for a, pW in self.model.actions(x, q).items()}
        if q == 0:
            best_action = max(V, key=lambda k: V.get(k)[0])
            return V[best_action][0], best_action
        else:
            return sum([ score * pW for (score, pW) in V.values()  ]), None

class GymAlphaBetaAgent(MultiAgentSearchAgent):
    def __init__(self, env, depth=1):
        super().__init__(env, depth)

    def multisearch_evaluate(self, state):
        return self.alpha_beta(state, 0, self.depth, alpha=-np.inf, beta=np.inf)

    def alpha_beta(self, x, q: int, d: int, alpha: float, beta: float):
        if d == 0 or self.model.terminal_test(x):
            return self.model.utility(x), None
        if q == 0:
            return self.MaxValue(x, q, d, alpha, beta)
        else:
            return self.MinValue(x, q, d, alpha, beta)

    def MaxValue(self, x, q, d, alpha, beta):
        maxScore = -np.inf
        maxAction = None

        for u in self.model.actions(x, q):
            successor = self.model.get_successor(x, u, q)
            score, _ = self.alpha_beta(successor, 1, d, alpha, beta)
            if score > beta:
                return score, u
            if score > maxScore:
                maxScore = score
                maxAction = u
                alpha = max(score, alpha)
        return maxScore, maxAction

    def MinValue(self, x, q, d, alpha, beta):
        q_ = (q + 1) % self.model.get_players(x) # next player q'
        d_ = d - 1 if q == x.getNumAgents() - 1 else d # next depth d'
        minScore = np.inf
        minAction = None

        for u in self.model.actions(x, q):
            successor = self.model.get_successor(x, u, q)
            score, _ = self.alpha_beta(successor, q_, d_, alpha, beta)
            if score < alpha:
                return score, u
            if score < minScore:
                minScore = score
                minAction = u
                beta = min(score, beta)
        return minScore, minAction


def gminmax(layout = 'ShipsLayout', Agent=None, depth=None, render=False, episodes=1, **kwargs):
    zoom = 2
    env = GymPacmanEnvironment(layout=layout, zoom=zoom, animate_movement=render, **kwargs)
    agent = Agent(env, depth=depth)
    stats, _ = train(env, agent, num_episodes=episodes, verbose=False)
    if episodes > 1:
        winp = np.mean( [s['Accumulated Reward']> 0 for s in stats] )
        print("Ran agent", Agent, "at depth", depth, "on problem", layout)
        print("Avg. win probability:", winp)
        print("Avg. search nodes expanded:", np.mean(agent.num_expanded), "\n")

    env.close()

def question_minimax(depth=1):
    start = time.time()
    gminmax(layout='ShipsLayout', Agent=GymMinimaxAgent, depth=depth, episodes=100)
    end = time.time()
    print("Elapsed time: ",end - start)

def question_expectimax(depth=3):
    start = time.time()
    gminmax(layout='ShipsLayout', Agent=GymExpectimaxAgent, depth=depth, episodes=100)
    end = time.time()
    print("Elapsed time: ", end - start)

def question_alphabeta(depth=3):
    start = time.time()
    gminmax(layout='ShipsLayout', Agent=GymAlphaBetaAgent, depth=depth, episodes=100)
    end = time.time()
    print("Elapsed time: ", end - start)

if __name__ == "__main__":
    depth = 3
    question_minimax(depth)
    question_expectimax(depth)
    question_alphabeta(depth)
