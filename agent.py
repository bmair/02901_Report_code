
import sys
import itertools
import numpy as np
from tqdm import tqdm
import os
from collections import OrderedDict, namedtuple

def existing_runs(experiment):
    nex = 0
    for root, dir, files in os.walk(experiment):
        if 'log.txt' in files:
            nex += 1
    return nex

def load_time_series(experiment_name, exclude_empty=True):
    """
    Load most recent non-empty time series (we load non-empty since lazylog creates a new dir immediately)
    """
    files = list(filter(os.path.isdir, glob.glob(experiment_name+"/*")))
    if exclude_empty:
        files = [f for f in files if os.path.exists(os.path.join(f, "log.txt")) and os.stat(os.path.join(f, "log.txt")).st_size > 0]

    if len(files) == 0:
        return [], None
    recent = sorted(files, key=lambda file: os.path.basename(file))[-1]
    stats = []
    with open(recent + '/log.txt', 'r') as f:
        csv_reader = csv.reader(f, delimiter='\t')
        for i, row in enumerate(csv_reader):
            if i == 0:
                head = row
            else:
                stats.append( {k:float(v) for k, v in zip(head, row) } )
    return stats, recent


def log_time_series(experiment, list_obs, max_xticks_to_log=None, run_name=None):
    logdir = f"{experiment}/"

    if max_xticks_to_log is not None and len(list_obs) > max_xticks_to_log:
        I = np.round(np.linspace(0, len(list_obs) - 1, max_xticks_to_log))
        list_obs = [o for i, o in enumerate(list_obs) if i in I.astype(np.int).tolist()]

    akeys = list(list_obs[0].keys())
    akeys += [k for k in list_obs[-1].keys() if k not in akeys]
    with LazyLog(logdir) as logz:
        for n,l in enumerate(list_obs):
            for k in akeys:
                if k not in l:
                    for ll in list_obs:
                        if k in ll:
                            v = ll[k]
                else:
                    v = l.get(k)
                logz.log_tabular(k,v)
            if "Steps" not in l:
                logz.log_tabular("Steps", n)
            if "Episode" not in l:
                logz.log_tabular("Episode",n)
            logz.dump_tabular(verbose=False)


class Agent: 
    """ Main agent class. See (Har20, \cite[Subsection 1.4.3)]{herlau} for additional details.  """
    def __init__(self, env): 
        self.env = env 

    def pi(self, s):
        """ Should return the Agent's action in state s (i.e. an element contained in env.action_space). Defaults to returning a random action """

        if hasattr(self.env, 'P') and self.env.action_space.n != len(self.env.P[s]):
            print("v. bad!")
        return self.env.action_space.sample()

    def train(self, s, a, r, sp, done=False): 
        """ Called at each step of the simulation.
        The agent was in state s, took action a, ended up in state sp (with reward r).
        'done' is a bool which indicates if the environment terminated when transitioning to sp. """
        pass 

    def __str__(self):
        """ A unique name for this agent. Used for plotting. """
        return super().__str__()

    def extra_stats(self):
        return {}

fields = ('time', 'state', 'action', 'reward')
Trajectory = namedtuple('Trajectory', fields)

def train(env, agent, experiment_name=None, num_episodes=1, verbose=True, reset=True, max_steps=1e10,
          max_runs=None, saveload_model=False, save_stats=True,
          return_trajectory=False, # Return the current trajectory
          resume_stats=None, # Resume stat collection from last save. None implies same as saveload_model
          temporal_policy=False, # if the policy depends on time
          ):
    """ Implement the main training loop, see (Har20, \cite[Subsection 1.4.4)]{herlau}.
    Function will train 'agent' on 'env' for the given number of episodes. You can (optionally) choose to save the result as 'experiment_name'
    for later plotting. The other features will be showcased in the exercises.
    """
    if max_runs is not None and existing_runs(experiment_name) >= max_runs:
            return experiment_name, None
    stats = []
    steps = 0
    ep_start = 0
    resume_stats = saveload_model if resume_stats is None else resume_stats

    if saveload_model:  # Code for loading/saving models
        did_load = agent.load(os.path.join(experiment_name))
    recent = None
    if resume_stats:
        stats, recent = load_time_series(experiment_name=experiment_name)
        if recent is not None:
            ep_start, steps = stats[-1]['Episode']+1, stats[-1]['Steps']

    trajectories = []
    with tqdm(total=num_episodes, disable=not verbose, file=sys.stdout) as tq:
        for i_episode in range(num_episodes): 
            s = env.reset() if reset else (env.s if hasattr(env, "s") else env.env.s) 
            time = 0
            reward = []
            trajectory = Trajectory(time=[], state=[], action=[], reward=[])
            for _ in itertools.count():
                a = agent.pi(s,time) if temporal_policy else agent.pi(s)
                sp, r, done, metadata = env.step(a)
                agent.train(s, a, r, sp, done)
                if return_trajectory:
                    trajectory.time.append(np.asarray(time))
                    trajectory.state.append(s)
                    trajectory.action.append(a)
                    trajectory.reward.append(np.asarray(r))

                reward.append(r)
                steps += 1
                time += metadata['dt'] if temporal_policy else 1
                if done or steps > max_steps:
                    trajectory.state.append(sp)
                    trajectory.time.append(np.asarray(time))
                    break
                s = sp 
            if return_trajectory:
                trajectory = Trajectory(**{field: np.stack([np.asarray(x_).reshape((-1,)) for x_ in getattr(trajectory, field)]) for field in fields})
                trajectories.append(trajectory)
            stats.append({"Episode": i_episode + ep_start,
                          "Accumulated Reward": sum(reward),
                          "Average Reward": np.mean(reward),
                          "Length": len(reward),
                          "Steps": steps, **agent.extra_stats()})
            tq.set_postfix(ordered_dict=OrderedDict(list(OrderedDict(stats[-1]).items() )[:5] ))
            tq.update()
    sys.stderr.flush()
    if saveload_model:
        agent.save(experiment_name)

    if resume_stats and save_stats and recent is not None:
        os.rename(recent+"/log.txt", recent+"/log2.txt")  # Shuffle old logs

    if experiment_name is not None and save_stats:
        log_time_series(experiment=experiment_name, list_obs=stats)
        print(f"Training completed. Logging {experiment_name}: '{', '.join( stats[0].keys()) }'")
    return stats, trajectories
